import { useState } from "react";
import useClipboard from "react-use-clipboard";

function HomePage() {
  const [alarms, setAlarms] = useState("");
  const [siteID, setSiteID] = useState([]);
  const regexes = {
    id: /[BETHNYKW]\d{4}/i,
  };

  const [isCopied, setCopied] = useClipboard(siteID.join("\n"), {
    successDuration: 2000,
  });

  function handleOnChange(e) {
    //changing TextArea value
    const value = e.target.value;
    setAlarms(value);

    //regexing and filtering site IDS
    let alarmList = value.split("\n");

    let siteIDList = alarmList.reduce((unique, alarm) => {
      const regexed = alarm.match(regexes.id);
      if (!regexed) {
        return unique;
      }
      return unique.includes(regexed[0]) ? unique : [...unique, regexed[0]];
    }, []);

    setSiteID(siteIDList);
  }

  return (
    <div className="flex overflow-hidden font-mono bg-gray-400">
      {/* <nav className="w-24 h-screen "></nav> */}
      <div className="w-1/2 h-screen">
        <textarea
          placeholder="Insert your alarm or text to process"
          className="
          w-full
          shadow-2xl
          h-full text-xs
          focus:border-none p-4
          leading-3 
          bg-gray-800
          text-gray-200
          focus:outline-none
          resize-none "
          value={alarms}
          onChange={handleOnChange}
        />
      </div>
      <div className="box-border relative w-1/2 h-screen bg-gray-900">
        <div className="flex flex-col absolute bottom-1/2 left-0 -ml-8">
          <button
            className={`rounded-md h-16 w-16 text-bold text-gray-200 focus:outline-none z-10 ${
              isCopied ? "bg-green-600" : "bg-gray-600"
            }`}
            onClick={setCopied}
          >
            {isCopied ? "Copied 👍" : "Copy"}
          </button>
          <button
            className="rounded-md h-16 w-16 bg-gray-600 text-gray-200 mt-8 focus:outline-none z-10"
            onClick={() => {
              setAlarms("");
              setSiteID([]);
            }}
          >
            Clear
          </button>
        </div>
        <h2 className="p-4 pl-8 text-lg font-bold text-gray-100 bg-gray-900 z-10 absolute top-0 left-0">
          {siteID.length ? `Found: ${siteID.length} Sites` : "Not Found"}
        </h2>
        {siteID ? (
          <textarea
            readOnly
            className="
            absolute top-0 left-0
            resize-none border-none focus:outline-none
            w-full h-full
            text-sm 
            pl-12 pt-16 p-8
            text-gray-200 
            leading-4 
            bg-transparent "
            value={siteID.join("\n")}
          />
        ) : null}
      </div>
    </div>
  );
}

export default HomePage;
